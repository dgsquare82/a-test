//
//  PhotoItemRaw.swift
//  A-Test
//
//  Created by Jae Kwang Lee on 2020/02/24.
//  Copyright © 2020 Jae Kwang Lee. All rights reserved.
//

import Foundation

struct PhotoItem: Codable {
    var id: String?
    var likes: Int?
    var description: String?
    
    var user: PhotoUser?
    var urls: PhotoURL?
}

struct PhotoUser: Codable {
    var id: String?
    var userName: String?
    var portfolio_url: String?
}

struct PhotoURL: Codable {
    var raw: String?
    var full: String?
    var regular: String?
    var small: String?
    var thumb: String?
}


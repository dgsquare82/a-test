//
//  ViewController.swift
//  A-Test
//
//  Created by Jae Kwang Lee on 2020/02/24.
//  Copyright © 2020 Jae Kwang Lee. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var photos = [PhotoItem]()

    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.tableView.register(UINib(nibName: "PhotoCell", bundle: nil), forCellReuseIdentifier: PhotoCell.identifier)
        loadData()
    }
    

    func loadData() {
        APIClient.sharedInstance().loadList(page: 0, limit: 10) { (response) in
            switch (response) {
            case .response(let jsonDict):
                guard let photos = try? self.decodePhotos(jsonDict: jsonDict) else {
                    self.showAlert(titie: NSLocalizedString("Error", comment: ""), message: NSLocalizedString("Decoding Error. Please contact the customer service.", comment: ""))
                    return
                }
                self.photos = photos
                self.tableView.reloadData()
            case .error(let error):
                self.showAlert(titie: NSLocalizedString("Error", comment: ""), message: error.localizedDescription)
            }
        }
    }
    
    fileprivate func decodePhotos(jsonDict: [[String: Any]]) throws -> [PhotoItem]  {
        let decoder = JSONDecoder()
        let jsonData = try JSONSerialization.data(withJSONObject: jsonDict, options: [])
        return try decoder.decode([PhotoItem].self, from: jsonData)
    }
    
    fileprivate func showAlert(titie: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(action)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
}

extension ViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.photos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell? = nil
        
        guard let photoCell = tableView.dequeueReusableCell(withIdentifier: PhotoCell.identifier, for: indexPath) as? PhotoCell else {
            return cell!
        }
        
        let photo = photos[indexPath.row]
        photoCell.cellModel = PhotoCellModel(photoURL: photo.urls?.small, desc: photo.description ?? "This image has no description.")
        return photoCell
    }
}

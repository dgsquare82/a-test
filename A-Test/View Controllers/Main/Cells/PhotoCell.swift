//
//  PhotoCell.swift
//  A-Test
//
//  Created by Jae Kwang Lee on 2020/02/24.
//  Copyright © 2020 Jae Kwang Lee. All rights reserved.
//

import UIKit
import AlamofireImage

struct PhotoCellModel {
    var photoURL: String?
    var desc: String
}


class PhotoCell: UITableViewCell {
    static let identifier: String = "PhotoCell"
    
    @IBOutlet weak var titleImageView: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    var cellModel: PhotoCellModel? = nil {
        didSet {
            self.updateUI()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func updateUI() {
        guard let model = self.cellModel else { return }
        
        descriptionLabel.text = model.desc
        titleImageView.image = UIImage(named: "place-holder")
        
        guard let photoURL = model.photoURL else {
            return
        }
        
        APIImageDownloader.sharedInstance().downloadImage(urlString: photoURL) { (res) in
            switch(res) {
            case .success(let imageRes):
                if (self.cellModel?.photoURL == imageRes.imageURL) {
                    self.titleImageView.image = imageRes.image
                }
            default:
                ()
            }
        }
        
    }
}

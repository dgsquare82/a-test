//
//  APIClient.swift
//  A-Test
//
//  Created by Jae Kwang Lee on 2020/02/24.
//  Copyright © 2020 Jae Kwang Lee. All rights reserved.
//

import Foundation
import Alamofire

enum APIResponse {
    case response([[String: Any]])
    case error(APIClientError)
}

enum APIClientError: Error {
    case noResponse
    case notValidFormat
    
    var localizedDescription: String {
        get {
            switch(self) {
            case .noResponse:
                return NSLocalizedString("Cannot access server. Please check the network.", comment: "")
            case .notValidFormat:
                return NSLocalizedString("Invalid Format. Please contact the customer service.", comment: "")
            }
        }
    }
}

class APIClient {
    let baseURLString: String
    
    class func sharedInstance() -> APIClient {
        struct __ { static let _sharedInstance = APIClient() }
        return __._sharedInstance
    }
    
    init() {
        self.baseURLString = "https://api.unsplash.com"
    }
    
    func loadList(page: Int = 0, limit: Int = 10, completion:((APIResponse) -> Void)?) {
        
        let urlString = "\(self.baseURLString)/photos"
        let parameters: [String: Any] = [
            "client_id": "L7cxM_R9mJ3xEqjZOnYar_UWzKWI99Yf9cU5BsmIp1o",
            "page": page,
            "limit": limit
        ]
        
        AF.request(urlString, parameters: parameters)
            .validate(statusCode: 200..<300)
            .responseJSON(completionHandler: { (response) in
                if let dictData = response.value as? [[String: Any]]  {
                    completion?(.response(dictData))
                }
                else {
                    completion?(.error(.notValidFormat))
                }
            })
    }
}

//
//  APIImageDownloader.swift
//  A-Test
//
//  Created by Jae Kwang Lee on 2020/02/24.
//  Copyright © 2020 Jae Kwang Lee. All rights reserved.
//

import Foundation
import AlamofireImage

struct ImageResponse {
    var image: UIImage
    var imageURL: String
}

class APIImageDownloader {
    class func sharedInstance() -> APIImageDownloader {
        struct __ { static let _sharedInstance = APIImageDownloader() }
        return __._sharedInstance
    }
    
    var imageDownloader: ImageDownloader!
    
    init() {
        imageDownloader = ImageDownloader(
            configuration: ImageDownloader.defaultURLSessionConfiguration(),
            downloadPrioritization: .fifo,
            maximumActiveDownloads: 4,
            imageCache: AutoPurgingImageCache()
        )
    }

    func downloadImage(urlString: String, completion: ((Result<(ImageResponse), Error>) -> Void)?) {
        let urlRequest = URLRequest(url: URL(string: urlString)!)

        imageDownloader.download(urlRequest) { response in
            debugPrint(response.result)

            switch (response.result) {
            case .success(let image):
                completion?(.success(ImageResponse(image: image, imageURL: urlString)))
            case .failure(let error):
                completion?(.failure(error))
            }
        }
    }
}


